package cashbox_main;

import cashbox_main.Controllers.MainStage_Controller;
import cashbox_main.Models.Constants;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

public class Main extends Application {
    private MainStage_Controller mainStage_controller;
    private static final String DEF_LANGUAGE = "cs";
    private static final String DEF_COUNTRY = "CZ";

    @Override
    public void start(Stage primaryStage) throws Exception{
        Locale.setDefault(new Locale(DEF_LANGUAGE, DEF_COUNTRY));
        FXMLLoader loader = new FXMLLoader(getClass().getResource("Views/MainStage_View.fxml"));
        Parent root = loader.load();
        mainStage_controller = loader.getController();

        primaryStage.setTitle("Simple cash box");
        primaryStage.setScene(new Scene(root));

        File f = new File(Constants.STYLES_PATH + "MainStage_Style.css"); //Connecting .css file
        primaryStage.getScene().getStylesheets().clear();
        primaryStage.getScene().getStylesheets().add("file:///" + f.getAbsolutePath().replace("\\", "/"));
        // TODO: 12. 5. 2019 Uncomment this later to enable logging
        //initLoginProcess();
        primaryStage.show();
        primaryStage.setResizable(false);

        mainStage_controller.loadSellItems();
    }


    public static void main(String[] args) {
        launch(args);
    }


    /**
     * Metoda se zavola hned pri spusteni aplikace a slouzi k inicializaci prihlasovani uzivatele
     */
    public void initLoginProcess() {
        Stage login_Stage = new Stage();
        Scene login_Scene = null;
        try {
            Parent login_root = FXMLLoader.load(getClass().getResource("Views/login/UserLogin_View.fxml"));
            login_Scene = new Scene(login_root);
        } catch (IOException e) {
            System.err.println("Chyba: Soubor rozhrani prihlaseni uzivatele nebyl nalezen.");
            e.printStackTrace();
            Platform.exit();
        }
        login_Stage.setTitle("Prihlášení uživatele");
        login_Stage.setScene(login_Scene);

        login_Stage.initModality(Modality.APPLICATION_MODAL); //Blokovani interakci se vsemi okny nez se uzivatel prihlasi
        login_Stage.showAndWait();
    }

    public MainStage_Controller getMainStage_controller() {
        return mainStage_controller;
    }
}
