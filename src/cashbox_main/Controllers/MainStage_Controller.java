package cashbox_main.Controllers;

import cashbox_main.Models.Constants;
import cashbox_main.Models.MainStage_Model;
import cashbox_main.Models.SellItem_Model;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

public class MainStage_Controller {
    private MainStage_Model dataModel = new MainStage_Model();

    @FXML
    private VBox mainContainer_CENTER;

    @FXML
    BorderPane main_BP;

    @FXML
    public void loadSellItems() {
        HBox loadedItemsRow = new HBox();
        loadedItemsRow.setId("itemsRow_Container");
        loadedItemsRow.setAlignment(Pos.CENTER);
        SellItem_Model[] sellItems = new SellItem_Model[Constants.MAX_SELL_ITEMS_PER_ROW];
        dataModel.loadSellItems(sellItems);
        // TODO: 6.5.2019 ObservableList for this data
        for (SellItem_Model sellItem : sellItems) {
            if (sellItem != null) {
                VBox itemDataContainer = new VBox();
                Button sellButton = new Button("Prodej");

                itemDataContainer.setStyle("-fx-border-color: black");
                itemDataContainer.getChildren().addAll(
                        sellItem.getItemImage(),
                        new Label("Název: " + sellItem.getName()),
                        new Label("Cena: " + sellItem.getPrice() + " Kč"),
                        new Label("Zbylo: " + sellItem.getLeft() + " ks."),
                        sellButton
                );
                loadedItemsRow.getChildren().add(itemDataContainer);
            } else
                break;
        }
        loadedItemsRow.setSpacing(15);
        mainContainer_CENTER.getChildren().add(loadedItemsRow);
    }


    @FXML
    public void openWarehouse() {
        Stage warehouse_Stage = new Stage();
        Scene warehouse_Scene = null;
        try {
            Parent warehouse_root = FXMLLoader.load(getClass().getResource("/warehouse/Warehouse_View.fxml"));
            warehouse_Scene = new Scene(warehouse_root);
        } catch (IOException e) {
            System.err.println("Chyba: Soubor vzledu okna skladu zboží nebyl nalezen.");
            e.printStackTrace();
            Platform.exit();
        }
        warehouse_Stage.setTitle("Sklad zboží");
        warehouse_Stage.setScene(warehouse_Scene);

        //Think if needed to restrict interaction with other windows.
        warehouse_Stage.initModality(Modality.APPLICATION_MODAL);
        warehouse_Stage.show();
    }

    @FXML
    void terminateApp() {
        Platform.exit();
    }
}
