package cashbox_main.Controllers;

import cashbox_main.Models.UserLogin_Model;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 * Trida odpovidajici za prihlasovani uzivatele do systemu
 */
public class UserLogin_Controller {
    private UserLogin_Model dataModel = new UserLogin_Model();
    @FXML
    private StackPane userLogin_SP;

    @FXML
    private Button exit_BT;

    @FXML
    private Button login_BT;

    @FXML
    private TextField username_TF;

    @FXML
    private TextField password_TF;

    @FXML
    void loginUser() {
        int dataCheckIndex = dataModel.verifyUserData(username_TF.getText(), password_TF.getText());
        if (dataCheckIndex == 1) {
            Alert success_alert = new Alert(Alert.AlertType.INFORMATION);
            success_alert.setTitle("Přihlášení uživatele");
            success_alert.setHeaderText("Byl(-a) jste úspěšně přihlášen(-a) do systému.");
            success_alert.showAndWait();

            Stage loginWindowStage = (Stage) userLogin_SP.getScene().getWindow();
            loginWindowStage.close();
        } else if (dataCheckIndex == 0) {
            Alert error_alert = new Alert(Alert.AlertType.ERROR);
            error_alert.setTitle("Přihlášení uživatele");
            error_alert.setHeaderText("Uvedené údaje nejsou spravné. Zkuste to prosím ještě jednou.");
            error_alert.showAndWait();
        }
    }

    // TODO: 07.05.2019 BUG is here, sending closing app request every time but do nothing until window is closed.
    @FXML
    void terminateApp() {
        Platform.exit();
    }
}
