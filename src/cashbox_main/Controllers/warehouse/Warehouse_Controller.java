package cashbox_main.Controllers.warehouse;

import cashbox_main.Models.warehouse.*;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.util.Comparator;

public class Warehouse_Controller {
    private Warehouse_Model dataModel;
    @FXML private TreeView<SellItem> sellItem_TV;
    @FXML private TextArea description_TA;
    @FXML private Label path_LB;
    @FXML private TextField price_TF;
    @FXML private Label details_LB;

    public void initialize() {
        dataModel = new Warehouse_Model();
        sellItem_TV.setCellFactory(treeView -> new SellItemDisplayCell());

        sellItem_TV.setEditable(true);
        sellItem_TV.getSelectionModel().selectedItemProperty().addListener((javafx.beans.value.ChangeListener<? super TreeItem<SellItem>>) (observable, oldValue, newValue) -> {
            path_LB.setText("Cesta k úzlu: " + buildNodePath(sellItem_TV));
            price_TF.setPromptText("Zadejte cenu položky. Editace se aplikuje na: " + sellItem_TV.getSelectionModel().getSelectedItem().getValue().getName());
            updateDetailsPane(sellItem_TV.getSelectionModel().getSelectedItem());
        });
        description_TA.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!description_TA.isDisable())
                updateNodeDescription(sellItem_TV.getSelectionModel().getSelectedItem());
        });
        sellItem_TV.setOnEditCommit(event -> sortChildren(event.getTreeItem().getParent()));

        sellItem_TV.setRoot(dataModel.getTreeViewRoot());
        sellItem_TV.getRoot().setExpanded(true);
        dataModel.getCategoryItems(sellItem_TV.getRoot());
        sortChildren(sellItem_TV.getRoot());
    }

    private String buildNodePath(TreeView<SellItem> treeView) {
        StringBuilder pathBuilder = new StringBuilder();
        for (TreeItem<SellItem> item = treeView.getSelectionModel().getSelectedItem();
             item != null; item = item.getParent()) {

            pathBuilder.insert(0, item.getValue().getName());
            if (item != treeView.getRoot()) pathBuilder.insert(0, " -> ");
        }
        return pathBuilder.toString();
    }

    private void updateDetailsPane(TreeItem<SellItem> selected) {
        if (selected.getValue().getType() == TreeItemType.ITEM) {
            details_LB.setText("Popis vybrané položky: " + selected.getValue().getName());
            description_TA.setText(selected.getValue().getDescription());
            description_TA.setDisable(false);
        } else {
            details_LB.setText("Vyberte položku pro editaci popisku:");
            description_TA.setText("Vyberte položku aby bylo možné změnit její popis.");
            description_TA.setDisable(true);
        }
    }

    private void updateNodeDescription(TreeItem<SellItem> selected) {
        selected.getValue().setDescription(description_TA.getText());
    }

    private void sortChildren(TreeItem<SellItem> parent) {
        if (parent != null) {
//            Collator coll = Collator.getInstance(Locale.getDefault());
//            coll.setStrength(Collator.PRIMARY);
//            parent.getChildren().sort(coll.compare(o1.getName(), o2.getName()));

            parent.getChildren().sort(Comparator.comparing(item -> item.getValue().getName()));
        }
    }

//    private int getInsertNodeIdx(String nodeName) {
//        if (sellItem_TV.getRoot().getValue().getName().equals(nodeName))
//            return sellItem_TV.getRoot().getChildren().indexOf();
//            if (!node.isLeaf()) {
//                node.setExpanded(true);
//                node.getChildren().forEach(this::expandAllNodes);
//            }
//    }

}
