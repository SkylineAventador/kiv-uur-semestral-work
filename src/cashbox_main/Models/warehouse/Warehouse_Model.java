package cashbox_main.Models.warehouse;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TreeItem;

import java.util.ArrayList;
import java.util.List;

public class Warehouse_Model {
    public TreeItem<SellItem> getTreeViewRoot() {
        return new TreeItem<SellItem>(new SellItem("Produkt"));
    }

    public void getCategoryItems(TreeItem<SellItem> parent) {
        parent.getChildren().addAll(
        new TreeItem<SellItem>(new SellItem("Zmrzlina")),
        new TreeItem<SellItem>(new SellItem("Nápoje")),
        new TreeItem<SellItem>(new SellItem("Ostatní"))
        );
        //test item - remove later
        parent.getChildren().get(0).getChildren().add(
                new TreeItem<SellItem>(new SellItem("Čokoládová", SellItemUnitType.UNIT, 20, 100, 100,
                        "Jemná a nezapomenutelná čokoládová chuť"))
        );
    }
}
