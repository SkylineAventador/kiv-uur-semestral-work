package cashbox_main.Models.warehouse;

public enum TreeItemType {
    CATEGORY, ITEM;

    @Override
    public String toString() {
        if (this == CATEGORY)
            return "Kategorie";
        else if (this == ITEM)
            return "Položka";
        return "Neznámý";
    }
}
