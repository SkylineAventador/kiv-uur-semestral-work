package cashbox_main.Models.warehouse;

public class SellItem implements Comparable<SellItem>{
    private String name, description;
    private int price, quantity, maxQuantity;
    private TreeItemType type;
    private SellItemUnitType unitType;

    /**
     * Constuctor for category item
     * @param name category name
     */
    public SellItem(String name) {
//        if (checkCorrectness_String(name, (byte) 1) && checkCorrectness_String(name, (byte) 2))
//            this.name = name;
//        else
//            throw new IllegalArgumentException("Name of a product or its type have an incorrect value.");
        this.name = name;
        this.description = "";
        this.maxQuantity = 0;
        this.type = TreeItemType.CATEGORY;
    }

    public SellItem(String name, SellItemUnitType unitType, int price, int quantity, int maxQuantity) throws IllegalArgumentException{
//        if (checkCorrectness_String(name, (byte) 1) && checkCorrectness_String(unit, (byte) 2)) {
//            this.name = name;
//            this.unit = unit;
//        } else
//            throw new IllegalArgumentException("Name of a product or its type have an incorrect value.");
        if (checkCorrectness_Digit(price) && checkCorrectness_Digit(quantity) && checkCorrectness_Digit(maxQuantity)) {
            this.price = price;
            this.quantity = quantity;
            this.maxQuantity = maxQuantity;
        } else
            throw new IllegalArgumentException("Price of a product or its quantity have an incorrect value.");
        this.name = name;
        this.unitType = unitType;
        this.description = "";
        this.type = TreeItemType.ITEM;
    }

    public SellItem(String name, SellItemUnitType unitType, int price, int quantity, int maxQuantity, String description) throws IllegalArgumentException{
//        if (checkCorrectness_String(name, (byte) 1) && checkCorrectness_String(unit, (byte) 2)) {
//            this.name = name;
//            this.unit = unit;
//        } else
//            throw new IllegalArgumentException("Name of a product or its type have an incorrect value.");
        if (checkCorrectness_Digit(price) && checkCorrectness_Digit(quantity) && checkCorrectness_Digit(maxQuantity)) {
            this.price = price;
            this.quantity = quantity;
            this.maxQuantity = maxQuantity;
        } else
            throw new IllegalArgumentException("Price of a product or its quantity have an incorrect value.");
        this.name = name;
        this.unitType = unitType;
        this.description = description;
        this.type = TreeItemType.ITEM;
    }

    /**
     * Function verifies if String argument consists of only a-z A-Z letters and 0-9 digits if argType is '1'.
     * If argType is '2' verifies if argument is among permitted constant type values.
     * @param argument value that is being verified.
     * @param argType verify type (see desc.)
     * @return true if argument matches condition, else otherwise.
     */
    @Deprecated
    private boolean checkCorrectness_String(String argument, byte argType) {
        if (argType == 1){
            // TODO: 25. 8. 2019 Solve this problem
            return argument.matches("^[\\p{Alnum}]+$");
        } else if (argType == 2) {
            return SellItemUnitType.valueOf(argument).toString().equals(argument);
        }
        return false;
    }

    private boolean checkCorrectness_Digit(int argument) {
        return argument < Integer.MAX_VALUE && argument > Integer.MIN_VALUE;
    }

    public String getName() {
        return name;
    }

    public TreeItemType getType() {
        return type;
    }

    public SellItemUnitType getUnitType() {
        return unitType;
    }

    public int getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getDescription() {
        return description;
    }

    public int getMaxQuantity() {
        return maxQuantity;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUnitType(SellItemUnitType unitType) {
        this.unitType = unitType;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int compareTo(SellItem o) {
        return this.getName().compareTo(o.getName());
    }
}
