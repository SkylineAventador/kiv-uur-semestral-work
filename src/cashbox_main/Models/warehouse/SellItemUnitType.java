package cashbox_main.Models.warehouse;

public enum SellItemUnitType {
    KILOGRAM,UNIT,LITER,POUND;

    /**
     * Vraci textovou reprezentaci hodnoty.
     * Pozor navratove hodnoty musi byt bez ceske diakritiky
     * @return textova reprezentace hodnoty
     */
    @Override
    public String toString() {
        switch (this){
            case UNIT: return "Kus";
            case LITER: return "Litr";
            case POUND: return "Libra";
            case KILOGRAM: return "Kilogram";
        }
        return "Neznámo";
    }
}
