package cashbox_main.Models;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class MainStage_Model {
    // TODO: 5. 5. 2019 Do not forget about observableArrayList or just a list
    private BufferedReader dataFileReader;
    private ObservableList<SellItem_Model> dataModel;

    public void loadSellItems(SellItem_Model[] sellItems) {
        String item_name = "Unknown";
        int item_left = -1, item_price = -1;
        Image item_image = new Image(Constants.NO_SELL_ITEM_IMAGE_PATH);

        String readLine = "";

        try {
            dataFileReader = new BufferedReader(new FileReader(Constants.SELL_ITEMS_DATA_PATH));
            readLine = dataFileReader.readLine();

            for (int i = 0; i < sellItems.length; i++) {
                    if (readLine == null || readLine.isEmpty())
                        break;
                    for (int j = 0; j < 4; j++) {
                        switch (j + 1) {
                            case 1:{
                                item_name = readLine.substring(readLine.indexOf(':') + 2);
                                readLine = dataFileReader.readLine();
                                break;
                            }
                            case 2:{
                                item_left = Integer.parseInt(readLine.substring(readLine.indexOf(':') + 2));
                                readLine = dataFileReader.readLine();
                                break;
                            }
                            case 3:{
                                item_price = Integer.parseInt(readLine.substring(readLine.indexOf(':') + 2));
                                readLine = dataFileReader.readLine();
                                break;
                            }
                            case 4:{
                                String imagePath = readLine.substring(readLine.indexOf(':') + 2);
                                if (!imagePath.isEmpty()) {
                                    item_image = new Image(Constants.MEDIA_PATH + imagePath);
                                } else {
                                    item_image = new Image(Constants.NO_SELL_ITEM_IMAGE_PATH);
                                }
                                readLine = dataFileReader.readLine();
                                break;
                            }
                        }
                    }
                    sellItems[i] = new SellItem_Model(item_name, item_left, item_price, new ImageView(item_image));
                    readLine = dataFileReader.readLine();//Skipping empty line, according to sell items file format
            }

            dataModel = FXCollections.observableArrayList(sellItems);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
