package cashbox_main.Models;

import javafx.scene.control.Alert;

import java.io.*;

public class UserLogin_Model {

    /**
     * Metoda overi prihlasovaci udaje zadane uzivatelem
     * @param username jmeno uzivatelskeho uctu k overeni
     * @param password heslo uzivatelskeho uctu k overeni
     * @return 1 - zadane udaje a ulozene v databazi se shoduji. 0 - nejsou shodne. -1 - ucet se zadanym jmenem neexistuje.
     */
    public int verifyUserData(String username, String password) {
        String database_username = "", database_password = "", currentReadString;

        try {
            File userData = loadUserDataFile(username);
            BufferedReader reader = new BufferedReader(new FileReader(userData));
            currentReadString = reader.readLine();
            while (currentReadString != null) {
                if (currentReadString.startsWith("Username:")) {
                    database_username = currentReadString.substring(currentReadString.indexOf(':') + 2);
                } else if (currentReadString.startsWith("Password:")) {
                    database_password = currentReadString.substring(currentReadString.indexOf(':') + 2);
                }
                if (!database_username.isEmpty() && !database_password.isEmpty()) //Kdyz potrebna data uz jsou, neni potreba nacitat dal.
                    break;
                else
                    currentReadString = reader.readLine();
            }
            reader.close(); //Zavreni streamu souboru.

            //Kdyz prokazana data se shoduji s daty v databazi, povolime prihlaseni.
            if(username.equals(database_username) && password.equals(database_password))
                return 1; //Vse v poradku
            else
                return 0; //Chybne udaje
        } catch (FileNotFoundException e) {
            //e.printStackTrace();
            Alert error_alert = new Alert(Alert.AlertType.ERROR);
            error_alert.setTitle("Přihlášení uživatele");
            error_alert.setHeaderText("Soubor s daty tohoto uživatele nebyl nalezen v databázi.");
            error_alert.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return -1; // Uzivatel nenalezen
    }

    private File loadUserDataFile(String username) throws FileNotFoundException{
        return (new File(Constants.USERSDATA_PATH + username + ".txt"));
    }
}
