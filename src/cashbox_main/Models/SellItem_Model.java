package cashbox_main.Models;

import javafx.scene.image.ImageView;

public class SellItem_Model {
    private String name;
    private int left;
    private int price;
    private ImageView itemImage;

    public SellItem_Model(String name, int left, int price, ImageView itemImage) {
        this.name = name;
        this.left = left;
        this.price = price;
        this.itemImage = itemImage;
    }

    public String getName() {
        return name;
    }

    public int getLeft() {
        return left;
    }

    public int getPrice() {
        return price;
    }

    public ImageView getItemImage() {
        return itemImage;
    }

}
