package cashbox_main.Models;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class DataModel {
    private ObservableList<SellItem_Model> sellItems;

    public DataModel(SellItem_Model[] sellItems) {
        if (sellItems != null)
            this.sellItems = FXCollections.observableArrayList(sellItems);
        else
            this.sellItems = FXCollections.observableArrayList();
    }

    public ObservableList<SellItem_Model> getSellItems() {
        return sellItems;
    }

    public SellItem_Model getSellItem(int index) {
        if (sellItems.get(index) != null) {
            return sellItems.get(index);
        }
        return sellItems.get(0);
    }
}
